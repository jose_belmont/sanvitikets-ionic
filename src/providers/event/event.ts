import { IResponse } from './../../interfaces/i-response';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { constants } from './../../constants';
import { Observable } from 'rxjs/Observable';
import { IEvent } from './../../interfaces/i-event';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the EventProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventProvider {

  constructor(public http: HttpClient) {
    console.log('Hello EventProvider Provider');
  }

  addEvent(event: IEvent): Observable<boolean> {
    // console.log(event);
    return this.http
      .post(`${constants.server}events`, event)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw('Error adding!')
      )
      .map((resp: IResponse) => {
        if (resp.error) {
          throw resp.errorMessage;
        }
        return true;
      });
  }

  

}
