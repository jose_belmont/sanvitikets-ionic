import { IResponse } from './../../interfaces/i-response';
import { IUser } from './../../interfaces/i-user';
import { constants } from './../../constants';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { HttpErrorResponse } from '@angular/common/http/src/response';
import { Observable } from 'rxjs/Observable';

import { Storage } from '@ionic/storage';
import { Observer } from 'rxjs/Observer';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  logged: boolean;
  private user: IUser;

  constructor(public http: HttpClient, private storage: Storage) {
  }

  login(user: IUser): Observable<boolean> {
    return this.http.post(`${constants.server}auth/login`, user)
      .map((response: IResponse) => {
        if (!response.error) {
          this.storage.set('token', response.token);
          this.logged = true;

          return true;
        }
        else {
          throw response.errorMessage;
        }
      }).catch(error => Observable.throw(error));
  }

  register(user: IUser): Observable<boolean> {
    return this.http
      .post(`${constants.server}auth/register`, user)
      .catch((resp: HttpErrorResponse) =>
        Observable.throw(
          'Error doing register!' +
          `. Server returned code ${resp.status}, message was: ${resp.message}`
        )
      )
      .map((resp: IResponse) => {
        if (resp.error) {
          throw resp.errorMessage;
        } else {
          return true;
        }
      });
  }
  
  isLogged(): Observable<boolean> {
    return Observable.create((observer: Observer<any>) => {

      this.storage.get('token').then(token => {
        this.http.get(`${constants.server}auth/token`).
          subscribe((resp: IResponse) => {
            if (!resp.error) {
              observer.next(true);
              observer.complete();
            } else {
              observer.next(false);
              observer.complete();
            }
          },
            error => {
              observer.next(false);
              observer.complete();
            }
          );

      });
    })
  }

  loginGoogle(user: IUser, token: string): Observable<boolean> {
    console.log('respgoogle:', token);
    this.storage.set('token', token);
    return this.http.get(`${constants.server}auth/google`)
      .catch((error: HttpErrorResponse) => Observable.throw(`Error trying to login google. Server returned ${error.message}`))
      .map((resp: IResponse) => {
        if (!resp.error) {
          this.storage.set('token', resp.token);
          this.user = resp.result;
          this.logged = true;

          return true;
        }
        throw resp.errorMessage;
      });
  }

  loginFacebook(user: IUser, respFacebook: any): Observable<boolean> {
    console.log('respFacebook:', respFacebook);
    this.storage.set('token', respFacebook.token);
    return this.http.get(`${constants.server}auth/facebook`)
      .catch((error: HttpErrorResponse) => Observable.throw(`Error trying to login google. Server returned ${error.message}`))
      .map((resp: IResponse) => {

        if (!resp.error) {
          this.storage.set('token', resp.token);
          this.user = resp.result;
          this.logged = true;

          return true;
        }
        throw resp.errorMessage;
      });
  }

  logout() {
    this.storage.remove('token');
    this.logged = false;

  }

}
