import { IUser } from './i-user';


export interface IEvent {
    id?: number,
    title: string,
    date: string | Date,
    description: string,
    image?: string,
    price: number,
    address: string,
    lat?: number,
    lng?: number,
    creator?: number,
    mine?: boolean,
    attend?: boolean,
    distance?: number,
    creatorData?: IUser
}