import { Camera, CameraOptions } from '@ionic-native/camera';
import { AuthProvider } from './../../providers/auth/auth';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUser } from './../../interfaces/i-user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { constants } from '../../constants';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  user: IUser = {
    image: "",
    lat: 0,
    lng: 0,
    name: "",
    email: "",
    password: "",
    password2: ""
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private http: HttpClient,
    private authProvider: AuthProvider,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
  private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  ionViewWillLoad() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.user.lat = resp.coords.latitude;
      this.user.lng = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  register() {
    if (this.user.password === this.user.password2) {
      this.authProvider.register(this.user).subscribe(
        () => this.navCtrl.setRoot('EventListPage'),
        (error) => this.showErrorRegister(error)
      );
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Password error or not match',
        subTitle: 'Password error or not match',
        buttons: ['Ok']
      });
      alert.present();
    }
  }
  cameraUse() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }
  private showErrorRegister(error) {
    let alert = this.alertCtrl.create({
      title: 'Login error',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

}
