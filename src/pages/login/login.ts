import { IUser } from './../../interfaces/i-user';

import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: IUser = {
    image: '',
    lat: 0,
    lng: 0,
    name: '',
    email: '',
    password: ''
  };

  constructor(public navCtrl: NavController, public authProvider: AuthProvider,
    public alertCtrl: AlertController, private geolocation: Geolocation) { }

  ionViewDidLoad() {
    this.authProvider.isLogged().subscribe(
      ok => {
        if(ok){
          this.navCtrl.setRoot('EventListPage');
        }
      }
    )
  }

  ionViewWillLoad() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.user.lat = resp.coords.latitude;
      this.user.lng = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  login() {
    this.authProvider.login(this.user)
      .subscribe(
        () => this.navCtrl.setRoot('EventListPage'),
        (error) => this.showErrorLogin(error)
      );
  }

  register() {
    this.navCtrl.push('RegisterPage');
  }

  private showErrorLogin(error) {
    let alert = this.alertCtrl.create({
      title: 'Login error',
      subTitle: error,
      buttons: ['Ok']
    });
    alert.present();
  }

}
